﻿using UnityEngine;
using System.Collections;

public class TitleArtAnimation : MonoBehaviour {

	//public variables
	public string gameTitle = "A Ludum Dare 28 Game";
	public GameObject titleLabel;
	public GameObject buttonPanel;
	public GameObject rose;
	public GameObject layer1, layer2, layer3, layer4, layer5, layer2a, layer2b, layer3a, layer3b, player;
	public GameObject titleCard, hardcorePanel, softcorePanel, titlePanel, hardcoreButtonsPanel, titleLogo;
	public AudioSource camaudio;
	//private variables
	private bool writingTitle;
	private float writeDelay = 0.1f;
	private int currentLetter = 0;
	
	void Start () {
		iTween.ScaleTo(gameObject, iTween.Hash("scale" , new Vector3(1,1, 1), "time", 6, "easetype", iTween.EaseType.easeInOutExpo, "oncomplete", "WriteTitle"));
		//iTween.ScaleTo(rose, iTween.Hash("scale" , new Vector3(20, 20, 1), "time", 6, "easetype", iTween.EaseType.easeInOutExpo, "oncomplete", "WriteTitle"));
		iTween.ValueTo(gameObject,  iTween.Hash("from", 0, "to", .55, "time", 8, "easetype", iTween.EaseType.easeOutQuad, "onupdate", "UpdateCameraFoV"));
		camaudio = Camera.main.GetComponent<AudioSource>();
	}

	void Update()
	{

		layer1.transform.position += new Vector3(-0.05f,0,0);
		layer2.transform.position += new Vector3(-0.02f,0,0);
		layer3.transform.position += new Vector3(-0.01f,0,0);
		layer4.transform.position += new Vector3(-0.0005f,0,0);
		//layer5.transform.position += new Vector3(-0.01f,0,0);
		
		if(layer1.transform.position.x - player.transform.position.x < -3)
		{
			layer1.transform.position = new Vector3(3, 0,0);
		}
		
		if(layer2a.transform.position.x - player.transform.position.x < -4.0f)
		{
			layer2a.transform.position = new Vector3(layer2b.transform.position.x + 4, 0,0);
		}
		
		if(layer2b.transform.position.x - player.transform.position.x < -4.0f)
		{
			layer2b.transform.position = new Vector3(layer2a.transform.position.x + 4, 0,0);
		}
		
		if(layer3a.transform.position.x - player.transform.position.x < -4.0f)
		{
			layer3a.transform.position = new Vector3(layer3b.transform.position.x + 4, 0,0);
		}
		
		if(layer3b.transform.position.x - player.transform.position.x < -4.0f)
		{
			layer3b.transform.position = new Vector3(layer3a.transform.position.x + 4, 0,0);
		}


		if(writingTitle)
		{
			writeDelay -= Time.deltaTime;
			if(writeDelay < 0)
			{
				WriteTitle();
				writeDelay = 0.1f;
			}
		}
	}
	void WriteTitle()
	{

		/*
		writingTitle = true;
		titleLabel.GetComponent<UILabel>().text = gameTitle.Substring(0,currentLetter);
		currentLetter++;
		if(currentLetter > gameTitle.Length)
		{
			writingTitle = false;
			FadeInButtonPanel();
		}
		*/
		titleLogo.SetActive(true);
		iTween.ScaleTo(titleLogo, iTween.Hash("scale" , new Vector3(1,1, 1), "time", 3, "easetype", iTween.EaseType.easeOutQuad));
		FadeInButtonPanel();
	}

	void FadeInButtonPanel()
	{
		buttonPanel.SetActive(true);
		iTween.ValueTo(gameObject,  iTween.Hash("from", 0, "to", 1, "time", 2, "easetype", iTween.EaseType.easeInExpo, "onupdate", "UpdateButtonPanelAlpha"));

	}

	void UpdateButtonPanelAlpha(float alpha)
	{
		buttonPanel.GetComponent<UIPanel>().alpha = alpha;
	}

	void UpdateCameraColor(Color color)
	{
		Camera.main.backgroundColor = color;
	}

	void UpdateCameraFoV(float fov)
	{
		Camera.main.orthographicSize = fov;
	}

	void UpdateHardcorePanelAlpha(float alpha)
	{
		hardcorePanel.GetComponent<UIPanel>().alpha = alpha;
	}

	void UpdateSoftcorePanelAlpha(float alpha)
	{
		softcorePanel.GetComponent<UIPanel>().alpha = alpha;
	}

	void UpdateTitlePanelAlpha(float alpha)
	{
		titlePanel.GetComponent<UIPanel>().alpha = alpha;
	}

	public void Back()
	{
		Application.LoadLevel("TitleMenu");
	}

	public void PrintHardcoreScreen()
	{
		iTween.ValueTo(gameObject,  iTween.Hash("from", 0, "to", 1, "time", 2, "easetype", iTween.EaseType.easeInExpo, "onupdate", "UpdateHardcorePanelAlpha"));
	}

	public void PrintSoftcoreScreen()
	{
		iTween.ValueTo(gameObject,  iTween.Hash("from", 0, "to", 1, "time", 2, "easetype", iTween.EaseType.easeInExpo, "onupdate", "UpdateSoftcorePanelAlpha"));
	}

	public void StartGame()
	{
		Application.LoadLevel("Introscene");
	}
	public void UpdateAudioVolume(float vol)
	{
		camaudio.volume = vol;
	}
	public void SoftcoreScreen()
	{
		titleCard.SetActive(true);
		softcorePanel.SetActive(true);
		buttonPanel.SetActive(false);
		iTween.ValueTo(gameObject,  iTween.Hash("from", 1, "to", 0, "time", 2, "easetype", iTween.EaseType.easeInExpo, "onupdate", "UpdateTitlePanelAlpha"));
		iTween.ValueTo(gameObject,  iTween.Hash("from", 1, "to", 0, "time", 2, "easetype", iTween.EaseType.easeInExpo, "onupdate", "UpdateButtonPanelAlpha"));
		iTween.ValueTo(gameObject,  iTween.Hash("from", 0.55f, "to", 0.00045f, "time", 5, "easetype", iTween.EaseType.easeInOutQuad, "onupdate", "UpdateCameraFoV", "oncomplete", "PrintSoftcoreScreen"));
		//iTween.ScaleTo(titleCard, iTween.Hash("scale" , new Vector3(1.24f,1.24f, 1), "time", 3, "easetype", iTween.EaseType.easeInOutExpo, "oncomplete", "HardcoreText"));
	}

	public void HardcoreScreen()
	{
		titleCard.SetActive(true);
		hardcorePanel.SetActive(true);
		buttonPanel.SetActive(false);
		iTween.ValueTo(gameObject,  iTween.Hash("from", 1, "to", 0, "time", 2, "easetype", iTween.EaseType.easeInExpo, "onupdate", "UpdateTitlePanelAlpha"));
		iTween.ValueTo(gameObject,  iTween.Hash("from", 1, "to", 0, "time", 2, "easetype", iTween.EaseType.easeInExpo, "onupdate", "UpdateButtonPanelAlpha"));
		iTween.ValueTo(gameObject,  iTween.Hash("from", 0.55f, "to", 0.00045f, "time", 5, "easetype", iTween.EaseType.easeInOutQuad, "onupdate", "UpdateCameraFoV", "oncomplete", "PrintHardcoreScreen"));
		//iTween.ScaleTo(titleCard, iTween.Hash("scale" , new Vector3(1.24f,1.24f, 1), "time", 3, "easetype", iTween.EaseType.easeInOutExpo, "oncomplete", "HardcoreText"));
	}

	public void StartSoftcoreGame()
	{
		iTween.ValueTo(gameObject,  iTween.Hash("from",1 , "to", 0, "time", 3, "easetype", iTween.EaseType.easeOutQuad, "onupdate", "UpdateSoftcorePanelAlpha"));
		
		iTween.ValueTo(gameObject,  iTween.Hash("from",1 , "to", 0, "time", 3, "easetype", iTween.EaseType.easeOutQuad, "onupdate", "UpdateAudioVolume", "oncomplete", "StartGame"));
		iTween.ValueTo(gameObject,  iTween.Hash("from",0.00045f , "to", 0.00005, "time", 3, "easetype", iTween.EaseType.easeOutQuad, "onupdate", "UpdateCameraFoV", "oncomplete", "StartGame"));
	}

	public void StartHardcoreGame()
	{
		PlayerPrefs.SetInt("Hardcore",1);
		iTween.ValueTo(gameObject,  iTween.Hash("from",1 , "to", 0, "time", 3, "easetype", iTween.EaseType.easeOutQuad, "onupdate", "UpdateHardcorePanelAlpha"));
		iTween.ValueTo(gameObject,  iTween.Hash("from",1 , "to", 0, "time", 3, "easetype", iTween.EaseType.easeOutQuad, "onupdate", "UpdateAudioVolume", "oncomplete", "StartGame"));
		iTween.ValueTo(gameObject,  iTween.Hash("from",0.00045f , "to", 0.00005, "time", 3, "easetype", iTween.EaseType.easeOutQuad, "onupdate", "UpdateCameraFoV", "oncomplete", "StartGame"));
	}
}
