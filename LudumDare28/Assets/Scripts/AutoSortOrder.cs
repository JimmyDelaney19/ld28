﻿using UnityEngine;
using System.Collections;

public class AutoSortOrder : MonoBehaviour {

	private SpriteRenderer sr;

	void Start () {
		sr = GetComponent<SpriteRenderer>();
	}

	void Update () {
		sr.sortingOrder = (int)((transform.position.y * -50) - 10.0f);
	}
}
