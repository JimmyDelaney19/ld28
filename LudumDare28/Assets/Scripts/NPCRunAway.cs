﻿using UnityEngine;
using System.Collections;

public class NPCRunAway : MonoBehaviour {
	UILabel dLabel;
	public string escapeTextMatch;
	public string locationWentToString;
	public GameObject TargetToMove;
	public Vector3 moveTo;
	public AudioClip doorOpen;
	public float displayTime;
	public GameObject TurnOff, TurnOn;


	private bool Active = true;
	// Use this for initialization
	void Start () {
		dLabel = DialogueManager.instance.DialogueLabel.GetComponent<UILabel>();
	}
	
	// Update is called once per frame
	void Update () {
	if(Active)
	{

		if(dLabel.text == escapeTextMatch)
		{
			iTween.CameraFadeAdd();
			iTween.CameraFadeTo(iTween.Hash("amount" ,1, "time", 2, "easetype", iTween.EaseType.easeOutQuad, "oncomplete", "Teleport", "oncompletetarget", gameObject));
			GameManager.instance.inquirePanel.SetActive(false);
		}
	}
	}
	
	void Teleport()
	{
		StartCoroutine(SwitchCharacterHolders());


		TargetToMove.transform.position = moveTo;
		iTween.CameraFadeAdd();
		iTween.CameraFadeTo(iTween.Hash("amount" ,0, "time", 2, "easetype", iTween.EaseType.easeOutQuad, "oncomplete", "DisplayText", "oncompletetarget", gameObject));
		Active = false;
		DialogueManager.instance.DialogueLabel.GetComponent<UILabel>().text = locationWentToString;
		GameManager.instance.player.audio.PlayOneShot(doorOpen);

	}
	
	void DisplayText()
	{
		Debug.Log("wat");
		//DialogueManager.instance.DialogueLabel.GetComponent<UILabel>().text = locationWentToString;
		GameManager.instance.currentState = GameManager.state.idle;
		DialogueManager.instance.DialoguePanel.SetActive(true);

		Invoke("TurnOffDialogueBox",displayTime);

		Invoke("TurnOnDialogueBox",2.0f);
		
	}

	public void TurnOnDialogueBox()
	{
		DialogueManager.instance.DialoguePanel.SetActive(true);
	}

	public void TurnOffDialogueBox()
	{

		
		DialogueManager.instance.DialoguePanel.SetActive(false);
	}

	IEnumerator SwitchCharacterHolders()
	{
		yield return new WaitForSeconds(3f);
		if(TurnOn != null)
		{
			//Activates next character holder
			TurnOn.SetActive(true);
			
			//after delay
			TurnOff.SetActive(false);
		}
		yield return null;
	}

}
