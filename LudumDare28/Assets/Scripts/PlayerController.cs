﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	public AnimationClip frankWalk;
	public AudioClip footstepSound;
	private Animator myAnim;
	public bool walking;
	private SpriteRenderer sr;
	public GameObject currentNPC;
	public AudioSource audioTest;

	
	private GameObject currentChar;
	private string index;
	private string inquiry, response;
	
	private int currentLength;
	private string currentOption;
	private enum Facing
	{
		up,
		left,
		right,
		down,
	}
	
	private Facing currentFacing;
	// Use this for initialization
	void Start () {

		myAnim = GetComponent<Animator>();
		sr = GetComponent<SpriteRenderer>();

	}
	
	// Update is called once per frame
	void Update () {


		sr.sortingOrder = (int)((transform.position.y * -50) - 10.0f);


		if(GameManager.instance.currentState == GameManager.state.movement)
		{
			MovementInput();
		}
		else if(GameManager.instance.currentState == GameManager.state.inquire)
		{
			if(GameManager.instance.inquirePanel.activeInHierarchy)
			{
				InquireInput();
			}
			else if(GameManager.instance.optionsPanel)
			{
				OptionsInput();
			}

		}
		else if(GameManager.instance.currentState == GameManager.state.options)
		{
		
		}
		else if(GameManager.instance.currentState == GameManager.state.dialogue)
		{
			OptionsInput();
		}
		else if(GameManager.instance.currentState == GameManager.state.idle)
		{
			CheckForMovement();
		}
	

	}

	void CheckForMovement()
	{
		if(Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow))
		{
			GameManager.instance.currentState = GameManager.state.movement;
		}
	}

	void BranchDialogue()
	{
		Debug.Log("YEAH");
		currentOption = currentOption.Substring((currentOption.IndexOf ("@")+1));
		Debug.Log(currentOption);
		currentChar = GameObject.Find(currentOption.Substring(0,currentOption.IndexOf("`")));
		currentOption = currentOption.Substring((currentOption.IndexOf("`") + 1));
		index = currentOption.Substring(0,1);
		Debug.Log (index);
		currentOption = currentOption.Substring((currentOption.IndexOf("`") + 1));
		inquiry = currentOption.Substring(0,currentOption.IndexOf("`"));
		Debug.Log (inquiry);
		currentOption = currentOption.Substring((currentOption.IndexOf("`") + 1));
		response = currentOption;
		
		DialogueManager.instance.UnlockInquiry(currentChar, int.Parse(index), inquiry, response);

		Debug.Log(response);
		Debug.Log(currentOption);

		currentOption = currentOption.Substring((currentOption.IndexOf("`") + 1));
		Debug.Log("JUST ADDED" + currentOption);

	}
	
	void OptionsInput()
	{
		if(Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
		{
			DialogueManager.instance.currentSelection++;
			DialogueManager.instance.currentSelection %= GameManager.instance.currentNPC.GetComponent<NPCDialogue>().responses.Count;
			DialogueManager.instance.Oarrow.transform.localPosition = new Vector3(-43, DialogueManager.instance.optionItems[DialogueManager.instance.currentSelection].transform.localPosition.y - 5, 1);
		}
		else if(Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow))
		{
			DialogueManager.instance.currentSelection--;
			DialogueManager.instance.currentSelection %= GameManager.instance.currentNPC.GetComponent<NPCDialogue>().responses.Count;
			if(DialogueManager.instance.currentSelection < 0)
			{
				DialogueManager.instance.currentSelection = GameManager.instance.currentNPC.GetComponent<NPCDialogue>().responses.Count - 1;
			}
			else
			{
				DialogueManager.instance.currentSelection %= GameManager.instance.currentNPC.GetComponent<NPCDialogue>().responses.Count;
			}
			DialogueManager.instance.Oarrow.transform.localPosition = new Vector3(-43, DialogueManager.instance.optionItems[DialogueManager.instance.currentSelection].transform.localPosition.y - 5, 1);
		}
		else if(Input.GetKeyDown(KeyCode.E))
		{
		
			Debug.Log(DialogueManager.instance.currentSelection);
			Debug.Log(DialogueManager.instance.optionItems.Length - 1);
			if(DialogueManager.instance.currentSelection == GameManager.instance.currentNPC.GetComponent<NPCDialogue>().inquiries.Count - 1)
			{
				GameManager.instance.inquirePanel.SetActive(true);
				DialogueManager.instance.PopulateInquirePanel();
				GameManager.instance.optionsPanel.SetActive(false);

			}
			else
			{

				DialogueManager.instance.PopulateInquirePanel();
				GameManager.instance.inquirePanel.SetActive(true);
				GameManager.instance.optionsPanel.SetActive(false);
				DialogueManager.instance.PopulateDialoguePanel(currentNPC);
				//Debug.Log(currentNPC);
				Invoke("TurnOffDialoguePanel", 6);

				if(GameManager.instance.currentNPC.GetComponent<NPCDialogue>().inquiries[DialogueManager.instance.currentSelection].Contains("`"))
				{
				
					
					currentOption = GameManager.instance.currentNPC.GetComponent<NPCDialogue>().inquiries[DialogueManager.instance.currentSelection];
					
					DialogueManager.instance.optionItems[DialogueManager.instance.currentSelection].GetComponent<UILabel>().text = currentOption.Substring(0,currentOption.IndexOf("`"));
					currentLength = currentOption.Length;
					
					currentOption = currentOption.Substring((currentOption.IndexOf("`") + 1));
					Debug.Log(currentOption.Substring(0,currentOption.IndexOf("`")));

			
					currentChar = GameObject.Find(currentOption.Substring(0,currentOption.IndexOf("`")));
					if(!currentChar)
					{
						Debug.Log("NO CHAR FOUND");
						return;
					}
					currentOption = currentOption.Substring((currentOption.IndexOf("`") + 1));
					index = currentOption.Substring(0,1);
					Debug.Log ("Index: " + index);
					currentOption = currentOption.Substring((currentOption.IndexOf("`") + 1));
					inquiry = currentOption.Substring(0,currentOption.IndexOf("`"));
					Debug.Log ("Inquiry: " + inquiry);
					currentOption = currentOption.Substring((currentOption.IndexOf("`") + 1));
			
					if(currentOption.Contains("`"))
					{
					//Beginning:
						response = currentOption.Substring(0,currentOption.IndexOf("`"));
						currentOption = currentOption.Substring((currentOption.IndexOf ("`")+1));
						inquiry = inquiry +"`"+ currentOption;
						Debug.Log("Respionse: " + response);
						if(!currentChar)
						{

							Debug.Log("NO Character found!");
							return;
						}
						DialogueManager.instance.UnlockInquiry(currentChar, int.Parse(index), inquiry, response);
						Debug.Log(currentOption);
						if(currentOption.StartsWith("@"))
						{
							BranchDialogue();

							if(currentOption.StartsWith("@"))
							{
								BranchDialogue();

								if(currentOption.StartsWith("@"))
								{
									BranchDialogue();
									
								}
								
							}

						}


					}
					else
					{

						Debug.Log(response);
						response = currentOption;
						DialogueManager.instance.UnlockInquiry(currentChar, int.Parse(index), inquiry, response);
					}

					

					Debug.Log (currentOption);



					//DialogueManager.instance.UnlockInquiry(currentChar, int.Parse(index), inquiry, response);
				}

				

			}
		}
		
	}

	void InquireInput()
	{
		if(Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow))
		{
			DialogueManager.instance.currentSelection++;
			DialogueManager.instance.currentSelection %= DialogueManager.instance.inquireItems.Length;
			DialogueManager.instance.Iarrow.transform.localPosition = new Vector3(-43, DialogueManager.instance.inquireItems[DialogueManager.instance.currentSelection].transform.localPosition.y - 6, 1);
		}
		else if(Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow))
		{
			DialogueManager.instance.currentSelection--;
			if(DialogueManager.instance.currentSelection < 0)
			{
				DialogueManager.instance.currentSelection = DialogueManager.instance.inquireItems.Length - 1;
			}
			else
			{
				DialogueManager.instance.currentSelection %= DialogueManager.instance.inquireItems.Length;
			}
			DialogueManager.instance.Iarrow.transform.localPosition = new Vector3(-43, DialogueManager.instance.inquireItems[DialogueManager.instance.currentSelection].transform.localPosition.y - 6, 1);
		}
		else if(Input.GetKeyDown(KeyCode.E))
		{
			if(DialogueManager.instance.currentSelection== DialogueManager.instance.inquireItems.Length - 1)
			{
				GameManager.instance.currentState = GameManager.state.idle;
				GameManager.instance.inquirePanel.SetActive(false);
				//DialogueManager.instance.currentSelection = 0;

			}
			else if(DialogueManager.instance.currentSelection== DialogueManager.instance.inquireItems.Length - 2)
			{
				if(!GameManager.murderHappened)
				{
					GameManager.instance.accusePanel.SetActive(true);
					iTween.ScaleTo(GameManager.instance.accusePanel, iTween.Hash("scale" , new Vector3(1,1, 1), "time", 3, "easetype", iTween.EaseType.easeInOutExpo));
					GameManager.instance.characterLabel.text = GameManager.instance.currentNPC.name;

				}
				else
				{
					//DialogueManager.instance.PopulateInquirePanel();
					GameManager.instance.inquirePanel.SetActive(true);
					DialogueManager.instance.DialoguePanel.SetActive(true);
					DialogueManager.instance.DialogueLabel.GetComponent<UILabel>().text = "Accuse me of what???";

					//Debug.Log(currentNPC);
					Invoke("TurnOffDialoguePanel", 6);
				}
			}
			else
			{
				//GameManager.instance.currentState = GameManager.state.options;
				GameManager.instance.inquirePanel.SetActive(false);
				GameManager.instance.optionsPanel.SetActive(true);
				DialogueManager.instance.PopulateOptionsPanel();
			}	
		}

	}

	void TurnOffDialoguePanel()
	{
		if(DialogueManager.instance.DialoguePanel.activeInHierarchy)
		{
			//DialogueManager.instance.DialoguePanel.SetActive(false);
		}
	}


	void MovementInput()
	{
		if(Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow))
		{
			audio.Play();
		}
		else if(Input.GetKeyUp(KeyCode.S) || Input.GetKeyUp(KeyCode.DownArrow) || Input.GetKeyUp(KeyCode.W) || Input.GetKeyUp(KeyCode.UpArrow) || Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.LeftArrow) || Input.GetKeyUp(KeyCode.D) || Input.GetKeyUp(KeyCode.RightArrow))
		{
			audio.Stop();
		}

		if(Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
		{
			rigidbody2D.AddForce(new Vector3(-17,0,0));
			currentFacing = Facing.left;
			transform.eulerAngles = new Vector3(0,0,0);

			myAnim.SetBool("Walking", true);
		}
		else if(Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
		{
			rigidbody2D.AddForce(new Vector3(17,0,0));
			currentFacing = Facing.right;
			transform.eulerAngles = new Vector3(0,180,0);
			myAnim.SetBool("Walking", true);
		}
		else if(Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W))
		{
			rigidbody2D.AddForce(new Vector3(0,17,0));
			currentFacing = Facing.up;
			myAnim.SetBool("Walking", true);
		}
		else if(Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S))
		{
			rigidbody2D.AddForce(new Vector3(0,-17,0));
			currentFacing = Facing.down;
			myAnim.SetBool("Walking", true);

		}
		else
		{
			myAnim.SetBool("Walking", false);
		}



		if(Input.GetKey(KeyCode.E))//Check to see if there is an object to interact with
		{

			if(currentFacing == Facing.left)
			{
			
				RaycastHit2D interactObject = Physics2D.Linecast(transform.position + new Vector3(0,-0.18f,0), transform.position + new Vector3(-0.19f,0,0) + new Vector3(0,-0.18f,0));

				if(interactObject && interactObject.collider.gameObject.tag == "NPC")
				{
					DialogueManager.instance.currentSelection = 0;
					GameManager.instance.inquirePanel.SetActive(true);
					DialogueManager.instance.PopulateInquirePanel();
					GameManager.instance.currentNPC = interactObject.collider.gameObject;
					GameManager.instance.currentState = GameManager.state.inquire;

				}
				else if(interactObject && interactObject.collider.gameObject.tag == "Clue")
				{
					DialogueManager.instance.DialoguePanel.SetActive(true);
					DialogueManager.instance.DialogueLabel.GetComponent<UILabel>().text = interactObject.collider.gameObject.GetComponent<ClueDialogue>().PickUpDialogue;

					if(interactObject.collider.gameObject.GetComponent<ClueDialogue>().charInquiry.Length > 0)
					{

						DialogueManager.instance.UnlockInquiry(interactObject.collider.gameObject.GetComponent<ClueDialogue>().character,
						                                       interactObject.collider.gameObject.GetComponent<ClueDialogue>().index,
						                                       interactObject.collider.gameObject.GetComponent<ClueDialogue>().charInquiry,
						                                       interactObject.collider.gameObject.GetComponent<ClueDialogue>().charResponse);
					}

					Invoke("TurnOffDialoguePanel",5);
					Destroy(interactObject.collider.gameObject);
					
				}
			}
			else if(currentFacing == Facing.right)
			{
				RaycastHit2D interactObject = Physics2D.Linecast(transform.position + new Vector3(0,-0.18f,0), transform.position + new Vector3(0.19f,0,0) + new Vector3(0,-0.18f,0));
				
				if(interactObject && interactObject.collider.gameObject.tag == "NPC")
				{
					DialogueManager.instance.currentSelection = 0;
					GameManager.instance.inquirePanel.SetActive(true);
					DialogueManager.instance.PopulateInquirePanel();
					GameManager.instance.currentNPC = interactObject.collider.gameObject;
					GameManager.instance.currentState = GameManager.state.inquire;

				}
				else if(interactObject && interactObject.collider.gameObject.tag == "Clue")
				{
					DialogueManager.instance.DialoguePanel.SetActive(true);
					DialogueManager.instance.DialogueLabel.GetComponent<UILabel>().text = interactObject.collider.gameObject.GetComponent<ClueDialogue>().PickUpDialogue;
					
					if(interactObject.collider.gameObject.GetComponent<ClueDialogue>().charInquiry.Length > 0)
					{
						
						DialogueManager.instance.UnlockInquiry(interactObject.collider.gameObject.GetComponent<ClueDialogue>().character,
						                                       interactObject.collider.gameObject.GetComponent<ClueDialogue>().index,
						                                       interactObject.collider.gameObject.GetComponent<ClueDialogue>().charInquiry,
						                                       interactObject.collider.gameObject.GetComponent<ClueDialogue>().charResponse);
					}
					
					Invoke("TurnOffDialoguePanel",5);
					Destroy(interactObject.collider.gameObject);
				}
			}
			else if(currentFacing == Facing.up)
			{
				RaycastHit2D interactObject = Physics2D.Linecast(transform.position + new Vector3(0,-0.18f,0), transform.position + new Vector3(0,0.05f,0) + new Vector3(0,-0.18f,0));
				
				if(interactObject && interactObject.collider.gameObject.tag == "NPC")
				{
					DialogueManager.instance.currentSelection = 0;
					GameManager.instance.inquirePanel.SetActive(true);
					DialogueManager.instance.PopulateInquirePanel();
					GameManager.instance.currentNPC = interactObject.collider.gameObject;
					GameManager.instance.currentState = GameManager.state.inquire;

				}
				else if(interactObject && interactObject.collider.gameObject.tag == "Clue")
				{
					DialogueManager.instance.DialoguePanel.SetActive(true);
					DialogueManager.instance.DialogueLabel.GetComponent<UILabel>().text = interactObject.collider.gameObject.GetComponent<ClueDialogue>().PickUpDialogue;
					
					if(interactObject.collider.gameObject.GetComponent<ClueDialogue>().charInquiry.Length > 0)
					{
						
						DialogueManager.instance.UnlockInquiry(interactObject.collider.gameObject.GetComponent<ClueDialogue>().character,
						                                       interactObject.collider.gameObject.GetComponent<ClueDialogue>().index,
						                                       interactObject.collider.gameObject.GetComponent<ClueDialogue>().charInquiry,
						                                       interactObject.collider.gameObject.GetComponent<ClueDialogue>().charResponse);
					}
					
					Invoke("TurnOffDialoguePanel",5);
					Destroy(interactObject.collider.gameObject);
				}
			}
			else if(currentFacing == Facing.down)
			{
				RaycastHit2D interactObject = Physics2D.Linecast(transform.position + new Vector3(0,-0.18f,0), transform.position + new Vector3(0,-0.05f,0) + new Vector3(0,-0.18f,0));
				
				if(interactObject && interactObject.collider.gameObject.tag == "NPC")
				{
					DialogueManager.instance.currentSelection = 0;
					GameManager.instance.inquirePanel.SetActive(true);
					DialogueManager.instance.PopulateInquirePanel();
					GameManager.instance.currentNPC = interactObject.collider.gameObject;
					GameManager.instance.currentState = GameManager.state.inquire;

				}
				else if(interactObject && interactObject.collider.gameObject.tag == "Clue")
				{
					DialogueManager.instance.DialoguePanel.SetActive(true);
					DialogueManager.instance.DialogueLabel.GetComponent<UILabel>().text = interactObject.collider.gameObject.GetComponent<ClueDialogue>().PickUpDialogue;
					
					if(interactObject.collider.gameObject.GetComponent<ClueDialogue>().charInquiry.Length > 0)
					{
						
						DialogueManager.instance.UnlockInquiry(interactObject.collider.gameObject.GetComponent<ClueDialogue>().character,
						                                       interactObject.collider.gameObject.GetComponent<ClueDialogue>().index,
						                                       interactObject.collider.gameObject.GetComponent<ClueDialogue>().charInquiry,
						                                       interactObject.collider.gameObject.GetComponent<ClueDialogue>().charResponse);
					}
					
					Invoke("TurnOffDialoguePanel",5);
					Destroy(interactObject.collider.gameObject);
				}
			}

		}
	}


}
