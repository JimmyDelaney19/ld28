﻿using UnityEngine;
using System.Collections;

public class PlaySoundCue : MonoBehaviour {
	public AudioClip soundCue;

	void PlaySound () {
		audio.PlayOneShot(soundCue);
	}
}
