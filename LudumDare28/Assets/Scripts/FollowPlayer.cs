﻿using UnityEngine;
using System.Collections;

public class FollowPlayer : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
		transform.position = new Vector3(Mathf.Lerp(transform.position.x, GameManager.instance.player.transform.position.x,1),transform.position.y, transform.position.z);
	}
}
