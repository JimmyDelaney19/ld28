﻿using UnityEngine;
using System.Collections;

public class LogoAnimation : MonoBehaviour {

	public AudioClip logoBling, trainHorn;
	public UIPanel panel;
	// Use this for initialization
	void Start () {
	

		audio.PlayOneShot(trainHorn);
		iTween.MoveTo(gameObject, iTween.Hash("path", iTweenPath.GetPath("LogoMovement"), "time", 4, "easetype", iTween.EaseType.easeInExpo));
		iTween.RotateAdd(gameObject, iTween.Hash("amount" ,new Vector3(0, 0, 7200), "time", 6, "easetype", iTween.EaseType.easeInOutExpo, "oncomplete", "Shake"));
	}
	
	public void Shake()
	{
		iTween.ValueTo(gameObject,  iTween.Hash("from", Color.black, "to", Color.white, "time", 1, "easetype", iTween.EaseType.linear, "onupdate", "UpdateSpriteColor", "oncomplete", "LogoFinished"));
		iTween.ValueTo(gameObject,  iTween.Hash("from", Color.white, "to", Color.black, "time", 1, "easetype", iTween.EaseType.linear, "onupdate", "UpdateCameraColor"));
	}

	public void LogoFinished()
	{
		audio.PlayOneShot(logoBling);
		Invoke("FadeOutCamera",1);
	}

	public void FadeOutCamera()
	{
		iTween.ValueTo(gameObject,  iTween.Hash("from", Color.white, "to", Color.black, "time", 3, "easetype", iTween.EaseType.linear, "onupdate", "UpdateSpriteColor", "oncomplete", "LoadTitleMenu"));
	}

	public void LoadTitleMenu()
	{
		if(PlayerPrefs.GetInt("Hardcore") == 1)
		{
			panel.gameObject.SetActive(true);
			return;
		}

		Application.LoadLevel("TitleMenu");
	}

	public void UpdateSpriteColor(Color color)
	{
		GetComponent<SpriteRenderer>().color = color;
	}

	public void UpdateCameraColor(Color color)
	{
		Camera.main.backgroundColor = color;
	}




}
