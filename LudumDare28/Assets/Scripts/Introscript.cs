﻿using UnityEngine;
using System.Collections;

public class Introscript : MonoBehaviour {
	public UILabel introLabel;
	public UIPanel panel, creditsPanel;
	public bool writingTitle;
	public float writeDelay;
	private int currentLetter = 0;
	public string gameTitle;
	public AudioClip typeWriter;
	// Use this for initialization

	void Start () {

		if(PlayerPrefs.GetString("success")== "true")
		{
			gameTitle = "You arrested Derran, and saved everyone. Great Job!";
		}
		else if(PlayerPrefs.GetString("success")== "false")
		{
			gameTitle = "You failed to arrest the true killer! Game Over";
		}
		else
		{
			gameTitle = "You are Frank Delaney, a hotshot investigator. Feeling particularly hard-boiled and restless, a loud crash from outside your cabin door brings you to your feet. Arrow Keys or WASD to move, E to interact.";
		}
		WriteIntro();

	}
	
	// Update is called once per frame
	void Update () {
	

		if(writingTitle)
		{
			writeDelay -= Time.deltaTime;
			if(writeDelay < 0)
			{
				WriteIntro();
				writeDelay = 0.075f;
				audio.pitch = Random.Range(0.5f,1);
				audio.PlayOneShot(typeWriter,Random.Range(0.5f,1));
			}
		}
	}

	void WriteIntro()
	{
		writingTitle = true;
		introLabel.text = gameTitle.Substring(0,currentLetter);
		currentLetter++;
		if(currentLetter > gameTitle.Length)
		{
			writingTitle = false;
			if(PlayerPrefs.GetString("success").Length > 0)
			{
				iTween.ValueTo(gameObject,  iTween.Hash("from", 0, "to", 1, "time", 2, "easetype", iTween.EaseType.easeInExpo, "onupdate", "CreditsPanelAlpha"));
				PlayerPrefs.DeleteKey("success");

			}
			else
			{

				iTween.ValueTo(gameObject,  iTween.Hash("from", 1, "to", 0, "time", 2, "easetype", iTween.EaseType.easeInExpo, "onupdate", "PanelAlpha", "oncomplete", "LoadGame"));
			}
		}
	}

	void LoadGame()
	{

		Application.LoadLevel("Game");
	}

	void PanelAlpha(float alpha)
	{
		panel.GetComponent<UIPanel>().alpha = alpha;
	}

	void CreditsPanelAlpha(float alpha)
	{
		creditsPanel.GetComponent<UIPanel>().alpha = alpha;
	}
}
