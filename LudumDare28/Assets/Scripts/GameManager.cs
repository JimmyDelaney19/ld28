﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {
	public static GameManager instance;

	public GameObject inquirePanel;
	public GameObject optionsPanel;
	public GameObject accusePanel;
	public GameObject currentNPC;
	public AudioClip[] gameMusic;
	public GameObject player;
	public GameObject layer1, layer2, layer2a, layer2b, layer3a, layer3b, layer3, layer4, layer5;
	public static bool murderHappened;
	public UILabel characterLabel;
	public enum state
	{
		movement,
		inquire,
		dialogue,
		inventory,
		options,
		idle

	}
	public state currentState;

	void Awake()
	{
		if(instance !=null)
		{
			GameObject.Destroy(instance);
		}
		else
		{
			instance = this;
		}
		player = GameObject.Find("Player");
		currentState = state.movement;

		//Camera.main
		GameObject.Find("Dining Cart").audio.clip = gameMusic[Random.Range(0,gameMusic.Length)];
		//Camera.main
		GameObject.Find("Dining Cart").audio.Play();

	}
	// Use this for initialization
	
	// Update is called once per frame
	void Update () {

		layer1.transform.position += new Vector3(-0.05f,0,0);
		layer2.transform.position += new Vector3(-0.02f,0,0);
		layer3.transform.position += new Vector3(-0.01f,0,0);
		layer4.transform.position += new Vector3(-0.0005f,0,0);
		//layer5.transform.position += new Vector3(-0.01f,0,0);

		if(layer1.transform.position.x - player.transform.position.x < -3)
		{
			layer1.transform.position = new Vector3(3, 0,0);
		}

		if(layer2a.transform.position.x - player.transform.position.x < -4.0f)
		{
			layer2a.transform.position = new Vector3(layer2b.transform.position.x + 4, 0,0);
		}

		if(layer2b.transform.position.x - player.transform.position.x < -4.0f)
		{
			layer2b.transform.position = new Vector3(layer2a.transform.position.x + 4, 0,0);
		}

		if(layer3a.transform.position.x - player.transform.position.x < -4.0f)
		{
			layer3a.transform.position = new Vector3(layer3b.transform.position.x + 4, 0,0);
		}
		
		if(layer3b.transform.position.x - player.transform.position.x < -4.0f)
		{
			layer3b.transform.position = new Vector3(layer3a.transform.position.x + 4, 0,0);
		}

		if(GameObject.Find("Dining Cart").audio.isPlaying == false)
		{
			
			GameObject.Find("Dining Cart").audio.clip = gameMusic[Random.Range(0,gameMusic.Length)];
			GameObject.Find("Dining Cart").audio.Play();
		}

		if(currentState == state.inquire)
		{
			//DisableAllPanels();
		
		}
		else if(currentState == state.options)
		{
			//DisableAllPanels();
			//inquirePanel.SetActive(false);
			//optionsPanel.SetActive(true);
			//DialogueManager.instance.PopulateOptionsPanel();
		}
		else if(currentState == state.dialogue)
		{
			//DisableAllPanels();

		}
		else if(currentState == state.idle)
		{
			//DialogueManager.instance.DialoguePanel.SetActive(false);
		}
		else if(currentState == state.movement)
		{
			//DisableAllPanels();
		}

	}

	void AccuseCharacter()
	{
		if(currentNPC.name == "Derran")
		{
			PlayerPrefs.SetString("success","true");

		}
		else 
		{
			PlayerPrefs.SetString("success","false");
		}
		iTween.CameraFadeAdd();
		iTween.CameraFadeTo(1, 4);
		iTween.ValueTo(gameObject,  iTween.Hash("from", 1, "to", 0, "time", 4, "easetype", iTween.EaseType.easeOutQuad, "onupdate", "UpdateAccusePanelAlpha", "oncomplete", "LoadLevel"));
		iTween.AudioTo(Camera.main.gameObject, iTween.Hash("volume", 1, "time", 4, "easetype", iTween.EaseType.easeOutQuad));
	}

	void LoadLevel()
	{
		Application.LoadLevel("CreditsScene");
	}

	void UpdateAccusePanelAlpha(float alpha)
	{

		accusePanel.GetComponent<UIPanel>().alpha = alpha;
	}

	void HideAccuseScreen()
	{

		iTween.ScaleTo(GameManager.instance.accusePanel, iTween.Hash("scale" , new Vector3(0.01f,0.01f, 1), "time", 3, "easetype", iTween.EaseType.easeInOutExpo, "oncomplete", "DisableAccuseScreen", "oncompletetarget", gameObject));

	}

	void DisableAccuseScreen()
	{
		GameManager.instance.accusePanel.SetActive(false);
	}
	void DisableAllPanels()
	{
		inquirePanel.SetActive(false);
		optionsPanel.SetActive(false);
	}
}
