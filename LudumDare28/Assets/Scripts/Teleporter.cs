﻿using UnityEngine;
using System.Collections;

public class Teleporter : MonoBehaviour {

	public GameObject teleportTo;
	public float xDistanceOffeset;
	public float yDistanceOffset;
	public AudioClip doorOpen;
	public enum Dir {up,down,left,right};
	public Dir Direction;


	void OnTriggerEnter2D(Collider2D other)
	{
		if(other.gameObject == GameManager.instance.player)
		{	
			if(Input.GetKey(Direction.ToString())){
				GameManager.instance.audio.PlayOneShot(doorOpen);
				iTween.CameraFadeAdd();
				iTween.CameraFadeTo(iTween.Hash("amount" ,1, "time", 1, "easetype", iTween.EaseType.easeOutQuad, "oncomplete", "Teleport", "oncompletetarget", gameObject));
			}
		}
	}

	void Teleport()
	{
		DialogueManager.instance.DialoguePanel.SetActive(false);
		GameManager.instance.player.transform.position = new Vector3(teleportTo.transform.position.x + teleportTo.GetComponent<Teleporter>().xDistanceOffeset, teleportTo.transform.position.y + teleportTo.GetComponent<Teleporter>().yDistanceOffset, teleportTo.transform.position.z);
		iTween.CameraFadeAdd();
		iTween.CameraFadeTo(iTween.Hash("amount" ,0, "time", 1, "easetype", iTween.EaseType.easeOutQuad));
	}
}
