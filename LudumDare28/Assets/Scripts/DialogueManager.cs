﻿using UnityEngine;
using System.Collections;

public class DialogueManager : MonoBehaviour {
	public static DialogueManager instance;
	public GameObject[] inquireItems;
	public GameObject[] optionItems;
	public GameObject[] dialogueItems;
	public GameObject[] inventoryItems;
	public GameObject Iarrow;
	public GameObject IDialogueBox;
	public GameObject Oarrow;
	public GameObject ODialogueBox;
	public GameObject DialoguePanel;
	public GameObject DialogueLabel;

	public int currentSelection;

	void Awake()
	{
		if(instance !=null)
		{
			GameObject.Destroy(instance);
		}
		else
		{
			instance = this;
		}

		currentSelection = 0;

	}
	// Update is called once per frame
	void Update () {
	
	}

	public void UnlockInquiry(GameObject character, int index, string inquiry, string response)
	{
		character.GetComponent<NPCDialogue>().inquiries[index] = inquiry;
		character.GetComponent<NPCDialogue>().responses[index] = response;

	}

	public void PopulateDialoguePanel(GameObject NPC)
	{
		//Iarrow.transform.localPosition = new Vector3(-43, inquireItems[0].transform.localPosition.y - 5, 1);

		IDialogueBox.transform.localScale = new Vector3(200, 32 * inquireItems.Length, 1);
		for(int i = 0; i < inquireItems.Length; i++)
		{
			inquireItems[i].transform.localPosition = new Vector3(-28,44f - (i * 29), 1);
		}

		//currentSelection++;
		currentSelection %= inquireItems.Length;
		Iarrow.transform.localPosition = new Vector3(-43, inquireItems[currentSelection].transform.localPosition.y - 6, 1);

		DialoguePanel.SetActive(true);
		DialogueLabel.GetComponent<UILabel>().text = GameManager.instance.currentNPC.name + ": " + GameManager.instance.currentNPC.GetComponent<NPCDialogue>().responses[currentSelection].ToString();
		
		
		
	}

	public void PopulateInquirePanel()
	{
		//Iarrow.transform.localPosition = new Vector3(-43, inquireItems[0].transform.localPosition.y - 5, 1);

		IDialogueBox.transform.localScale = new Vector3(200, 32 * inquireItems.Length, 1);
		for(int i = 0; i < inquireItems.Length; i++)
		{
			inquireItems[i].transform.localPosition = new Vector3(-28,44 - (i * 29), 1);
		}

		//currentSelection++;
		currentSelection %= inquireItems.Length;
		Iarrow.transform.localPosition = new Vector3(-43, inquireItems[currentSelection].transform.localPosition.y - 6, 1);




	}

	public void PopulateOptionsPanel()
	{
		ODialogueBox.transform.localScale = new Vector3(200, 32 * GameManager.instance.currentNPC.GetComponent<NPCDialogue>().inquiries.Count, 1);


		for(int i = 0; i < GameManager.instance.currentNPC.GetComponent<NPCDialogue>().inquiries.Count; i++)
		{
			string currentOption;
			optionItems[i].SetActive(true);
			currentOption = GameManager.instance.currentNPC.GetComponent<NPCDialogue>().inquiries[i];
			if(currentOption.Contains("`"))
			{
				GameObject currentChar;
				string index;
				string inquiry, response;

				int currentLength;
				optionItems[i].GetComponent<UILabel>().text = currentOption.Substring(0,currentOption.IndexOf("`"));
				currentLength = currentOption.Length;
			




				//GameObject currentChar = currentOption.currentOption.IndexOf("`");


			}
			else
			{
				optionItems[i].GetComponent<UILabel>().text = currentOption;

			}

			optionItems[i].transform.localPosition = new Vector3(-28,50 - (i * 29), 1);
		}

		if(GameManager.instance.currentNPC.GetComponent<NPCDialogue>().inquiries.Count < optionItems.Length)
		{
			for(int i = optionItems.Length - GameManager.instance.currentNPC.GetComponent<NPCDialogue>().inquiries.Count; i > 0; i--)
			{
				optionItems[optionItems.Length-i].SetActive(false);
			}
		}

		currentSelection %= GameManager.instance.currentNPC.GetComponent<NPCDialogue>().inquiries.Count;
		Oarrow.transform.localPosition = new Vector3(-43, optionItems[currentSelection].transform.localPosition.y - 5, 1);

	}
}
